/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */
#include "sht31.h"
#include "i2c_if.h"

static struct {
    unsigned char buf[5];

    eDeviceStatus_t* pstatus;
    sht31_data* pdata;
} sht31;

#define SHT31_ADDRESS_L   0x44
#define SHT31_ADDRESS_H   0x45

static const uint8_t SHT31_ADDRESS = (SHT31_ADDRESS_H << 1);

static const uint8_t CMD_READ_STATUS_REG[] = { 0xF3, 0x2D };
static const uint8_t CMD_MEASURE_T[]     = { 0x24, 0x00 };
static const uint8_t DELAY_MEASURE_T     = 15;

static uint8_t sht31_generate_crc(uint8_t* data, uint16_t count);
static inline uint8_t sht31_check_crc(uint8_t* data, uint16_t count);

#define I2C_TIMEOUT   (500)

static void init_func();
static void read_func();

static void init_func()
{
	sht31.buf[0] = CMD_READ_STATUS_REG[0];
	sht31.buf[1] = CMD_READ_STATUS_REG[1];

	HAL_I2C_Master_Transmit(&hi2c1, SHT31_ADDRESS, sht31.buf, 2, I2C_TIMEOUT);
	HAL_I2C_Master_Receive(&hi2c1, SHT31_ADDRESS, sht31.buf, 3, I2C_TIMEOUT);
	if (sht31_check_crc(sht31.buf, 2))
	{
		*sht31.pstatus = eDevice_Initialized;
	} else
	{
		*sht31.pstatus = eDevice_Failed;
	}
}

static void read_func()
{
	// Trigger measurement
	sht31.buf[0] = CMD_MEASURE_T[0];
	sht31.buf[1] = CMD_MEASURE_T[1];
	HAL_I2C_Master_Transmit(&hi2c1, SHT31_ADDRESS, sht31.buf, 2, I2C_TIMEOUT);

	// wait for conversion to complete
	HAL_Delay(DELAY_MEASURE_T);

	HAL_I2C_Master_Receive(&hi2c1, SHT31_ADDRESS, sht31.buf, 6, I2C_TIMEOUT);

	if (sht31_check_crc(sht31.buf, 2) && sht31_check_crc(sht31.buf + 3, 2)) {
		int32_t temp_ticks = (sht31.buf[1] & 0xFF) | (sht31.buf[0] << 8);
		int32_t rh_ticks = (sht31.buf[4] & 0xFF) | (sht31.buf[3] << 8);

		/**
		* formulas for conversion of the sensor signals, optimized for fixed point algebra:
		* Temperature       = 175 * S_T / 2^16 - 45
		* Relative Humidity = 100 * S_RH / 2^16
		*/
		sht31.pdata->temp = (((21875 * temp_ticks) >> 13) - 45000);
		sht31.pdata->hum = ((12500 * rh_ticks) >> 13);

		*sht31.pstatus = eDevice_Ok;
	}
	else {
		*sht31.pstatus = eDevice_Failed;
	}
}


static const uint8_t CRC_POLYNOMIAL    = 0x31;
static const uint8_t CRC_INIT          = 0xff;

static uint8_t sht31_check_crc(uint8_t* data, uint16_t count) {
    return sht31_generate_crc(data, count) == data[count];
}

static uint8_t sht31_generate_crc(uint8_t* data, uint16_t count) {
    uint8_t crc = CRC_INIT;
    uint8_t current_byte;
    uint8_t crc_bit;

    /* calculates 8-Bit checksum with given polynomial */
    for (current_byte = 0; current_byte < count; ++current_byte) {
        crc ^= (data[current_byte]);
        for (crc_bit = 8; crc_bit > 0; --crc_bit) {
            if (crc & 0x80) {
                crc = (crc << 1) ^ CRC_POLYNOMIAL;
            } else {
                crc = (crc << 1);
            }
        }
    }
    return crc;
}

void sht31_init(eDeviceStatus_t* pstatus) {
    sht31.pstatus = pstatus;
    init_func();
}

void sht31_read(eDeviceStatus_t* pstatus, sht31_data* pdata) {
    sht31.pstatus = pstatus;
    sht31.pdata = pdata;
    read_func();
}

/*
 * I2C_if.c
 *
 *  Created on: Dec 20, 2022
 *      Author: User
 */

#include "i2c_if.h"
#include "stm32wlxx_nucleo_errno.h"

/* Private variables ---------------------------------------------------------*/
/**
  * @brief  TX complete callback
  * @return none
  */
static void (*TxCpltCallback)(void *);
/**
  * @brief  RX complete callback
  * @param  rxChar ptr of chars buffer sent by user
  * @param  size buffer size
  * @param  error errorcode
  * @return none
  */
static void (*RxCpltCallback)(void *);

/* Public function ---------------------------------------------------------*/

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  /* USER CODE BEGIN HAL_UART_TxCpltCallback_1 */

  /* USER CODE END HAL_UART_TxCpltCallback_1 */
  /* buffer transmission complete*/
  if (hi2c->Instance == I2C1)
  {
    TxCpltCallback(NULL);
  }
  /* USER CODE BEGIN HAL_UART_TxCpltCallback_2 */

  /* USER CODE END HAL_UART_TxCpltCallback_2 */
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  /* USER CODE BEGIN HAL_UART_TxCpltCallback_1 */

  /* USER CODE END HAL_UART_TxCpltCallback_1 */
  /* buffer transmission complete*/
  if (hi2c->Instance == I2C1)
  {
    RxCpltCallback(NULL);
  }
  /* USER CODE BEGIN HAL_UART_TxCpltCallback_2 */

  /* USER CODE END HAL_UART_TxCpltCallback_2 */
}

void I2C_Register_TxCallback(void *cb)
{
	TxCpltCallback = cb;
}

void I2C_Register_RxCallback(void *cb)
{
	RxCpltCallback = cb;
}

uint8_t I2C_xfer(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,
        uint16_t wSize, uint16_t rSize, uint16_t timeout)
{
	if (HAL_I2C_Master_Transmit(hi2c, DevAddress, pData, wSize, timeout) == HAL_OK)
	{
		return HAL_I2C_Master_Receive(hi2c, DevAddress, pData, rSize, timeout);
	}
	else return HAL_ERROR;
}

void I2C_resume()
{
	if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
	Error_Handler();
  }
}
